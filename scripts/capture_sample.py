# Blender import
import bpy
import math
import bmesh

import numpy as np
import numpy.linalg as LA
import random
import os

pi = 3.141592

def look_at(target):
    ray = np.subtract(target, camera.location)
    ray_xy = np.array([ray[0], ray[1], 0])
    x = np.array([-ray[2], +0, ray[1]])
    y = np.array([LA.norm(ray_xy), 0, -ray[0]])
    camera.rotation_euler = np.arctan2(y, x)


def render(filepath='img/', filename='look_at.png'):
    bpy.ops.render.render()
    bpy.data.images['Render Result'].save_render(filepath + filename)


# オブジェクトの参照
target_obj =  bpy.data.objects['Suzanne']
target_obj.rotation_euler = (3.14,0,0.0)


camera = bpy.data.objects['Camera']
lamp = bpy.data.objects['Light']

# レンダリングカメラ設定
bpy.context.scene.camera = bpy.data.objects["Camera"]
path = os.environ['HOME'] + "/blender_ws/test/"

# 配置設定
lamp.location = (0,-20,0)
lamp.rotation_euler = (pi/2.0,0,0.0)
lamp_data = lamp.data
lamp_data.color = (1.0,1.0,1.0)
lamp_data.energy = 10000

# ループ設定
look_at_point = [0, 0.0, 0]

for i in range(10):
    x = random.uniform(-0.3, 0.3)
    y = random.uniform(0, -0.3) - 3
    z = random.uniform(-0.3, 0.3)
    camera.location = (x,y,z)
    
    gb_color = random.uniform(0.3, 1.0)
    lamp_data.color = (1.0, gb_color, gb_color)

    euler_z = random.uniform(-1.57, 1.57)
    target_obj.rotation_euler = (0.0, 0.0, euler_z)

    look_at(look_at_point) # 設定点の向きにカメラを向ける
    render(filename=f'render{i}.png', filepath=path)




