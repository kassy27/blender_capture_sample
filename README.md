# OverView

blender capture sample

# How to Test sample

* make directory for saving images

```
mkdir ~/blender_ws/test
```

* put suzanne model on blender

* run script on blender's scripting tab

# Environment

* Ubuntu18.04
* blender3.1.2